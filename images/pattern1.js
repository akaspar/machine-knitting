#!/bin/sh
':' //; exec "$(command -v nodejs || command -v node)" --use_strict "$0" "$@"
"use strict";

const DATFile = require('./datfile.js');

let dat = new DATFile();
dat.allocatePixels(20, 20);
for(let x = 0; x < 20; ++x){
  for(let y = 0; y < 20; ++y){
    dat.setPixel(y, x, 1 + (y % 2));
  }
}

dat.write('pattern1.dat');
