#!/bin/sh
':' //; exec "$(command -v nodejs || command -v node)" --use_strict "$0" "$@"
"use strict";

// --- modules
const fs = require('fs');
const path = require('path');
const PNG = require('png-js');
const DATFile = require('./datfile.js');

let imageFile = process.argv[2]; // input image
if(!imageFile){
  console.log('Usage: ./png2dat.js file.png [file.dat]');
  process.exit(0);
}
let datFile  = process.argv[3]; // reference image file
if(!datFile){
  datFile = imageFile.replace(/.png/i, '') + '.dat';
}
try {
  if(fs.lstatSync(datFile).isDirectory()){
    datFile = path.join(datFile, path.basename(imageFile).replace(/.png/i, '.dat'));
  }
} catch(e){}

console.log('From ' + imageFile + ' to ' + datFile);

const pix2int = (r, g, b) => ((r & 0xFF) << 16) + ((g & 0xFF) << 8) + (b & 0xFF);
// create mapping to get DAT values
let palette = DATFile.getDefaultPalette(true);
let colormap = {};
for(let i = 0; i < 255; ++i){
  let r = palette[i];
  let g = palette[i + 0x100];
  let b = palette[i + 0x200];
  colormap[pix2int(r, g, b)] = i;
}

// read image and generate DAT file

let png = PNG.load(imageFile);
png.decode(data => {

  const width = png.width;
  const height = png.height;

  let dat = new DATFile();
  dat.allocatePixels(width, height);
  for(let i = 0, j = 0; i < data.length; i += 4, ++j){
    // find instruction
    let r = data[i + 0];
    let g = data[i + 1];
    let b = data[i + 2];
    let instr = colormap[pix2int(r, g, b)];
    // invert y axis
    let x = j % width;
    let y = Math.floor(j / width);
    let iy = height - 1 - y;
    dat.data[width * iy + x] = instr || 0;
  }
  dat.write(datFile);

});
