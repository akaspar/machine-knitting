#!/bin/sh
':' //; exec "$(command -v nodejs || command -v node)" --use_strict "$0" "$@"
"use strict";

const DATFile = require('./datfile.js');

let dat = new DATFile();
dat.create(10);
dat.writePreamble(); // newline=true (because streaming from create)
for(let i = 0; i < 10; ++i){
  dat.setLine(dat.current, 1 + (i%2), { R3: 6 }, i % 2 ? 'right' : 'left', true);
}
dat.writePostamble();
dat.setLineDirections();
dat.setCarrierPositions();
dat.write('test2.dat');
