#!/bin/sh
':' //; exec "$(command -v nodejs || command -v node)" --use_strict "$0" "$@"
"use strict";

const DATFile = require('./datfile.js');

let dat = new DATFile();
dat.allocateLines(10, 10, true);
for(let i = 0; i < 10; ++i){
  dat.setLine(i + 3, 1 + (i%2), { R3: 6 }, i % 2 ? 'right' : 'left');
}
dat.setLineDirections();
dat.setCarrierPositions();
dat.write('test1.dat');
