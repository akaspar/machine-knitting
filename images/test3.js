#!/bin/sh
':' //; exec "$(command -v nodejs || command -v node)" --use_strict "$0" "$@"
"use strict";

const DATFile = require('./datfile.js');

let dat = new DATFile();
dat.create(10);
dat.writePreamble(); // newline=true (because streaming from create)

// create welt for hookup
for(let i = 1; i < 10; i += 2)
  dat.setValue(dat.current, i, 11); // tuck
dat.setValue(dat.current, 0, 216);  // float
dat.setLineOptions(dat.current, { R3: 5, R10: 5, R15: 10 });
dat.newLine();

for(let i = 0; i < 10; i+= 2)
  dat.setValue(dat.current, i, 11); // tuck
dat.setValue(dat.current, 9, 216);  // float
dat.setLineOptions(dat.current, { R3: 5, R6: 33 });
dat.newLine();

// knitting
for(let i = 0; i < 2; ++i)
  dat.setLine(dat.current, 51, { R3: 5, R6: 5 });

// cutting
dat.setSegment(dat.current, 9, [16], { R3: 255, R6: 5, R15: 90 }); // R15=90 for end of yarn init

// knitting
for(let i = 0; i < 2; ++i)
  dat.setLine(dat.current, 51, { R3: 5, R5: i == 0 ? 2 : 0, R6: 5 }); // reset carrier (R5) on first line

// transfer + knitting
for(let i = 0; i < 3; ++i){
  // transfer
  dat.setSegment(dat.current, 4, [20, 20], { R5: 1 });
  dat.setSegment(dat.current, 4, [30], { L2: 1, L4: 10, R5: 1 });
  dat.setSegment(dat.current, 5, [30], { L4: 11, R5: 1 });
  // knit
  dat.setLine(dat.current, 51, { R3: 5, R6: 5 });
  dat.setLine(dat.current, 51, { R3: 5, R6: 5 });
}

// end knitting
dat.setLine(dat.current, 51, { R3: 5, R6: 5 });
dat.setLine(dat.current, 51, { R3: 5, R6: 5, R10: 105, R15: 20 });

dat.writePostamble();
dat.setLineDirections();
dat.setCarrierPositions();
dat.write('test3.dat');
