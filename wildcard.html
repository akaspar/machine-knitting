<!doctype html>
<html>
<title>Introduction to Machine Knitting | Wildcard</title>

<xmp theme="simplex">
TA: Alexandre Kaspar and Paloma GR

This is a brief introduction to industrial machine knitting. It is given in two parts: (1) an introduction to machine knitting and programming, (2) a manufacturing session where you fabricate your personal design on the machine.

**Number of students**: 5-8 <br>
**Where**: dry lab of 32-321 <br>
**When**: depending on student interest and availability, defaulting to

* Fri 12/7 for the [introduction](index.html) session (1pm to ~3pm)
* Mon 12/10 and/or Tue 12/11 for the *fabrication* session

# Getting there
**When**: on Friday 12/07, 1pm to 3pm (given [availability](https://www.when2meet.com/?7353568-wFweo))<br>
**Where**: in the couch area of 32-321

Since it's not trivial to find, we'll be meeting 15min before the session at the elevators near the Gates entrance of Stata center ([building 32](http://whereis.mit.edu/?go=32)).

<img src="images/meeting_place.png" height="400">

# What will I be using?
The knitting machine is a Shima Seiki WholeGarment [SWG091N2](http://www.shimaseiki.com/product/knit/swg_n2/index3.html) with 15 gauge needles (i.e. 15 needles per inch).

The provided material will be mostly acrylic yarn, specifically [Tamm 2/30](http://www.knitknackshop.com/Tamm-Carts/Tamm/4200.htm), which we have in a few different colors.

<img src="images/machine_side.jpg" width="224" height="300">
<img src="images/machine_bed_-_Edited.jpg" width="400" height="300">

The machine in action (casting off the needles):

<video src="images/castoff_compressed.mp4" controls></video>

# What will I do with the machine?

Three options, choose one:

1. Color pattern on two-sided flat sheet of fabric
2. Make-your-own knit pattern
3. Patterned templates (*experimental)

## Option 1: two or three colors on two-sided "Jacquard" fabric

[Jacquard knit](https://vintagefashionguild.org/fabric-resource/jacquard-knit/) fabric uses the idea of [Jacquard weaving](https://en.wikipedia.org/wiki/Jacquard_loom) with looms, and applies it to knitted fabric.
This means the pattern is alternating between two sides (front and back), which allows easy patterning on one side (and constrained patterning on the other), with multiple colors.
 
An example of 3-colors two-sided fabric (front and back):

<img src="images/knitted_tablemat_-_Edited.jpg" width="300" height="266">
<img src="images/knitted_tablemat_back_-_Edited.jpg" width="300" height="266">

## Option 2: knitting patterns

Knitting allows intricate texture patterns to be made by combining local operations.
This can be done on any type of fabric, although we suggest doing that on either a flat structure or a simple tubular structure.

You can get inspirations for your pattern(s) [online](https://www.knittingstitchpatterns.com/) (many websites and books on stitch patterns), or you can design your own.

An example of tubular structure with patterns:

<img src="images/knitted_shape1.jpg" width="224" height="300">
<img src="images/knitted_shape2.jpg" width="224" height="300">
<img src="images/k3-min.jpg" width="224" height="300">

## Option 3: patterned templates (*experimental)

You will have the option to work with an experimental system for designing your own shapes onto which you can program arbitrary patterns. The shaping part is taken care by assembling basic primitives, and the patterning is done using a simple select-and-apply language based on Javascript.

**Note**: the "experimental" part comes from the fact that you'll be using a work-in-progress design tool written in the browser. Simple small structures should work reasonably well, but more complex ones (such as the glove below) may require some debugging.

Examples of knitted structures with the templating tool:

<img src="images/knitting_glove.gif" width="224" height="300">
<img src="images/knitted_cat.jpg" width="300" height="300">
<img src="images/knitted_mannequin_-_Edited__1_.jpg" width="224" height="300">

# How will I program my sample?
The programming environment is basically a time-needle image, which you have to write from scratch (or from given templates), where each color corresponds to a specific machine instruction (with several potential options and settings on the sides).
As examples, there are the core part of the programs for the glove and the cat shown before:

<img src="images/glove_instructions.png" width="300" height="266">
<img src="images/knitcat.png" width="400" height="266">

You will probably not be making the full glove, but you can definitely do interesting knitted structures and patterns.
However, note that the backend machine software (used for compiling knit structures) is proprietary, thus you'll instead be provided with a small API to write these programs in [Node.js](https://nodejs.org/en/) (options 1 and 2) or access to the work-in-progress design tool (option 3).

If you choose option 1, you'll mainly be automatically translating a multi-layered image into a knit structure. Option 2 will have you write your own knitting program for hand-designed patterns.
Option 3 will have you create a more complex shape (and patterns on top of it).

# Why knitting instead of ...

* **3d printing**: you can knit much larger structure within the time you'd 3d print them, and it's much sparser / lighter
* **weaving**: knitted fabric can stretch a lot (up to 100%) whereas woven fabric typically stretches much less (~15%); furthermore, you can knit general 3d shapes, whereas looms are typically restrained to flat fabric which then needs to be composed with cutting and sewing

# What should I read if I am interested?
Introductory:

* [MAS.865 Fiber/Knitting-Weaving](http://fab.cba.mit.edu/classes/MAS.865/fiber/knitting-weaving.html)
* [The design of 3D shape knitted preforms](https://researchbank.rmit.edu.au/view/rmit:6130): notably the background (part 1), i.e. chapters 2, 3 and 4

Specialized:

* [A Compiler for 3D Machine Knitting](https://www.disneyresearch.com/publication/machine-knitting-compiler/), Siggraph 2016, McCann et al
* [Automatic Machine Knitting of 3D Meshes](https://textiles-lab.github.io/publications/2018-autoknit/), Siggraph 2018, Narayanan et al
* [Efficient Transfer Planning for Flat Knitting](https://textiles-lab.github.io/publications/2018-flat-xfer-plan/), [SCF 2018](http://scf.acm.org/2018), Lin et al

# Signup

**Attending Friday**:

| Name   | Option of Interest (1, 2, 3) |
| ------ | ------ |
| Yuping | 1 | 
| Nicole Adler | 2 |
| Emily Hsu | 2 | 
| Jim Peraino | 1 |
| Molly Mason | 1 | 
| Sinan Goral | 1 |
| Kate Spies | 1 |
| Daniel Estandian | 2.5 |

**Cannot attend Friday**:

* Leilani Gilpin (2.5)
* Amnahir (1.5)

Contact Alexandre and Paloma: `{akaspar,palomagr}@mit.edu` and provide us with (1) what you want to do for this week, and (2) when you are available for each session.


# Miscellaneous Resources
[Accessibility](http://accessibility.mit.edu/) @ [MIT](http://web.mit.edu) [CSAIL](http://csail.mit.edu)
</xmp>
<script src="sd/strapdown.js"></script>
</html>
